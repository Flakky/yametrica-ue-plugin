// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "UObject/Object.h"
#include "AppMetrica.generated.h"

UCLASS()
class APPMETRICA_API UAppMetrica : public UObject
{

	GENERATED_BODY()


public:

	UFUNCTION(BlueprintCallable, Category="AppMetrica")
	static void			ReportAnalyticsEvent				(FString EventName);

	UFUNCTION(BlueprintCallable, Category = "AppMetrica")
	static void			ReportAnalyticsEventParams			(FString EventName, FString Params);

	UFUNCTION(BlueprintCallable, Category = "AppMetrica")
	static void			ReportAnalyticsError				(FString Error);

	UFUNCTION(BlueprintCallable, Category = "AppMetrica")
	static void			SendAnalyticsBuffer					();

	UFUNCTION(BlueprintPure, Category="AppMetrica")
	static FString		StringAnalyticsValue				(FString ParamName, FString Value);

	UFUNCTION(BlueprintPure, Category="AppMetrica")
	static FString		NumberAnalyticsValue				(FString ParamName, float Value);

	UFUNCTION(BlueprintPure, Category="AppMetrica")
	static FString		BoolAnalyticsValue					(FString ParamName, bool Value);

	UFUNCTION(BlueprintPure, Category="AppMetrica")
	static FString		ParamsAnalyticsValue				(FString ParamName, FString Params);

	UFUNCTION(BlueprintPure, Category="AppMetrica")
	static FString		BuildAnalyticsParams				(TArray<FString> Params);

	UFUNCTION(BlueprintCallable, Category = "AppMetrica")
	static void			SetProfileInfo						(FString Params);
};
