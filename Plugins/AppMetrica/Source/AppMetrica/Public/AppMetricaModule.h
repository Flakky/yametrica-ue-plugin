// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FAppMetricaModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void		StartupModule			() override;
	virtual void		ShutdownModule			() override;

	static void			SendEvent				(FString EventName);
};
