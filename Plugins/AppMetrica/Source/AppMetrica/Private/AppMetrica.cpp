// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "AppMetrica.h"

#if PLATFORM_ANDROID
#include "Android/AndroidJNI.h"
#include "Android/AndroidApplication.h"

#define APPMETRICA_CLASS_NAME "com/yandex/metrica/YandexMetrica"
#define REVENUE_CLASS_NAME "com/yandex/metrica/Revenue"
#endif

#define LOG_TAG "AppMetrica"

void UAppMetrica::ReportAnalyticsEvent(FString EventName)
{
#if PLATFORM_ANDROID
	JNIEnv *Env = FAndroidApplication::GetJavaEnv();
	jclass jClass = FAndroidApplication::FindJavaClass(APPMETRICA_CLASS_NAME);

	if (jClass) {
		const char *strMethod = "reportEvent";

		jmethodID jMethod = Env->GetStaticMethodID(jClass, strMethod, "(Ljava/lang/String;)V");

		if (jMethod) {
			jstring eventName = Env->NewStringUTF(TCHAR_TO_UTF8(*EventName));

			Env->CallStaticVoidMethod(jClass, jMethod, eventName);
			__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "AppMetrica Method Call Successful %s", strMethod);
		}
		else {
			__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Failed to find method %s", strMethod);
		}

		Env->DeleteLocalRef(jClass);
}
	else {
		__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Failed to find class %s", APPMETRICA_CLASS_NAME);
	}
#endif
}

void UAppMetrica::ReportAnalyticsEventParams(FString EventName, FString Params)
{
#if PLATFORM_ANDROID
	JNIEnv *Env = FAndroidApplication::GetJavaEnv();
	jclass jClass = FAndroidApplication::FindJavaClass(APPMETRICA_CLASS_NAME);

	if (jClass) {
		const char *strMethod = "reportEvent";

		jmethodID jMethod = Env->GetStaticMethodID(jClass, strMethod, "(Ljava/lang/String;Ljava/lang/String;)V");

		if (jMethod) {
			jstring eventName = Env->NewStringUTF(TCHAR_TO_UTF8(*EventName));
			jstring propertiesArg = Env->NewStringUTF(TCHAR_TO_UTF8(*Params));

			Env->CallStaticVoidMethod(jClass, jMethod, eventName, propertiesArg);
			__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "AppMetrica Method Call Successful %s", strMethod);
		}
		else {
			__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Failed to find method %s", strMethod);
		}

		Env->DeleteLocalRef(jClass);
	}
	else {
		__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Failed to find class %s", APPMETRICA_CLASS_NAME);
	}
#endif
}

void UAppMetrica::ReportAnalyticsError(FString Error)
{
#if PLATFORM_ANDROID
	JNIEnv *Env = FAndroidApplication::GetJavaEnv();
	jclass jClass = FAndroidApplication::FindJavaClass(APPMETRICA_CLASS_NAME);

	if (jClass) {
		const char *strMethod = "reportEvent";

		jmethodID jMethod = Env->GetStaticMethodID(jClass, strMethod, "(Ljava/lang/String;)V");

		if (jMethod) {
			jstring eventName = Env->NewStringUTF(TCHAR_TO_UTF8(*Error));

			Env->CallStaticVoidMethod(jClass, jMethod, eventName);
			__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "AppMetrica Method Call Successful %s", strMethod);
		}
		else {
			__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Failed to find method %s", strMethod);
		}

		Env->DeleteLocalRef(jClass);
	}
	else {
		__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Failed to find class %s", APPMETRICA_CLASS_NAME);
	}
#endif
}

void UAppMetrica::SendAnalyticsBuffer()
{
#if PLATFORM_ANDROID
	JNIEnv *Env = FAndroidApplication::GetJavaEnv();
	jclass jClass = FAndroidApplication::FindJavaClass(APPMETRICA_CLASS_NAME);

	if (jClass) {
		const char *strMethod = "sendEventsBuffer";

		jmethodID jMethod = Env->GetStaticMethodID(jClass, strMethod, "()V");

		if (jMethod) {
			Env->CallStaticVoidMethod(jClass, jMethod);
			__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "AppMetrica Method Call Successful %s", strMethod);
		}
		else {
			__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Failed to find method %s", strMethod);
		}

		Env->DeleteLocalRef(jClass);
	}
	else {
		__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Failed to find class %s", APPMETRICA_CLASS_NAME);
	}
#endif
}

void UAppMetrica::SetProfileInfo(FString Params)
{
#if PLATFORM_ANDROID
	JNIEnv *Env = FAndroidApplication::GetJavaEnv();

		const char *strMethod = "AndroidThunkJava_setProfile";

		jmethodID jMethod = Env->GetMethodID(FJavaWrapper::GameActivityClassID, strMethod, "(Ljava/lang/String;)V");

		if (jMethod) {
			jstring profileInfo = Env->NewStringUTF(TCHAR_TO_UTF8(*Params));

			FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, jMethod, profileInfo);

			__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "AppMetrica Method Call Successful %s", strMethod);
		}
		else {
			__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "Failed to find method %s", strMethod);
		}
#endif
}

FString	UAppMetrica::StringAnalyticsValue(FString ParamName, FString Value)
{
	return "\""+ ParamName + "\":\"" + Value + "\"";
}

FString	UAppMetrica::NumberAnalyticsValue(FString ParamName, float Value)
{
	return "\"" + ParamName + "\":" + FString::SanitizeFloat(Value);
}

FString	UAppMetrica::BoolAnalyticsValue(FString ParamName, bool Value)
{
	return "\"" + ParamName + "\":" + (Value?"true":"false");
}

FString	UAppMetrica::ParamsAnalyticsValue(FString ParamName, FString Params)
{
	return "\"" + ParamName + "\":" + Params;
}

FString UAppMetrica::BuildAnalyticsParams(TArray<FString> Params)
{
	FString Out = "{";

	for (int p=Params.Num()-1;p>=0;p--)
	{
		Out += Params[p];
		if (p!=0) Out += ",";
	}

	Out+="}";

	return Out;
}
