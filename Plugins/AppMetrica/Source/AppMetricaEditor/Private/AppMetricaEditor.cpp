#include "AppMetricaEditor.h"

#include "Modules/ModuleInterface.h"
#include "ISettingsModule.h"
#include "Modules/ModuleManager.h"

#include "AppMetricaProjectSettings.h"

#define LOCTEXT_NAMESPACE "AppMetricaPlugin"

void FAppMetricaEditor::StartupModule()
{
    ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings");
    if( SettingsModule != nullptr )
    {
        SettingsModule->RegisterSettings(
                                         "Project", "Plugins", "AppMetrica",
                                         LOCTEXT( "AppMetricaSettingsName", "AppMetrica" ),
                                         LOCTEXT( "AppMetricaSettingsDescription", "AppMetrica settings" ),
                                         GetMutableDefault< UAppMetricaProjectSettings >()
                                         );
    }
}

void FAppMetricaEditor::ShutdownModule()
{
    ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings");
    if( SettingsModule != nullptr )
    {
        SettingsModule->UnregisterSettings( "Project", "Plugins", "AppMetrica" );
    }
}

IMPLEMENT_MODULE(FAppMetricaEditor, AppMetricaEditor)

#undef LOCTEXT_NAMESPACE
