#pragma once

#include "Modules/ModuleManager.h"

/**
* The public interface to this module
*/
class FAppMetricaEditor : public IModuleInterface
{

public:
    /** IModuleInterface implementation */
    void StartupModule();
    void ShutdownModule();
};