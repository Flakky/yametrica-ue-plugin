#pragma once

#include "AppMetricaProjectSettings.generated.h"

UCLASS(config=Engine, defaultconfig)
class UAppMetricaProjectSettings : public UObject
{
    GENERATED_UCLASS_BODY()

public:
    UPROPERTY(Config, EditAnywhere, Category=IosSetup, Meta=(ToolTip="AppMetrica Secret Key"))
    FString AppMetricaSecretKey;
};
